# **Viva Init script**

## Description

Script that you'll find in this repository will initially kicks off your system configuration to be able to work with Viva Billing.

## Usage

Run script without parameters. Script will ask you for configuration interactively.
You should run 'viva-init.sh' script as **root**

```
#!

# ./viva-init.sh

```

While processing script will ask some parameters interactively.
You need to know information about you credentials for Viva DB connection.

Example of script output:

```
#!

# ./viva-init.sh 
This script will install packages needed for Viva
It can take a while....
Do you want to override Viva Database Credentials [Y/N]:y
Type new values for DB connection, or press enter to keep old values
Database host IP or DNS [1.1.1.1]: 1.1.1.2
Database port (dafault 5432) [5432]:6543
Database name [5]: database_name
Database user [5]: user
Database password [5***]:
Generating .vivapgpass...

```
NOTICE: Script will not display any characters when you'll type your password!

## Supported Operating Systems


```
#!
Linux:
  CentOS 6 release
  CentOS 7 release
  Rocky 8 release

```

