#!/bin/sh

set -e

PUPPET_IP='mgmt.billing-viva.com'
PUPPET_PORT='8140'

#Check OS
if [ `uname` == 'Linux' ]; then
    FACTS_DIR='/etc/facter/facts.d'
    VIVA_HOME='/var/lib/vivabill'
    API_AUTH_CONF="$VIVA_HOME/vivabill.json"
elif [ `uname` == 'FreeBSD' ]; then
    FACTS_DIR="/etc/facter/facts.d"
    VIVA_HOME='/usr/local/etc/vivabill'
    API_AUTH_CONF="$VIVA_HOME/vivabill.json"
else
    exit 1
fi

#Check distro and set puppet env
if [ `cat /etc/os-release | head -n 1 | cut -f 2 -d '"' | cut -f 1 -d ' '` == 'Rocky' ]; then
    PUPPET_ENV='rocky8'
elif [ `cat /etc/os-release | head -n 1 | cut -f 2 -d '"' | cut -f 1 -d ' '` == 'CentOS' ]; then
    PUPPET_ENV="vivabillpuppet6"
else
    exit 1
fi

fail()
{
  echo "$1"
  exit 1
}

check_root()
{
  if [ `whoami` != 'root' ]; then
    fail "This script must be run as root"
  fi
}

install_puppet()
{
  printf "Configuring OS\nIt can take a while....\n"
  case `uname` in
  Linux)
    printf "Current system based on linux, checking dist\n"
    if [ -f /etc/centos-release ]; then
      release=`grep -o '[0-9]*' /etc/centos-release | head -n 1`
      if ! $(rpm -qa | grep -q 'puppet6-release') ; then
        curl -k -o puppet6-release-el-${release}.noarch.rpm https://yum.puppet.com/puppet6-release-el-${release}.noarch.rpm
        rpm -Uvh puppet6-release-el-${release}.noarch.rpm || fail "Error while installing Repo RPM"
      fi
      yum install -y epel-release
      yum install -y jq
      yum install puppet-agent -y >/dev/null|| fail "Error while installing Puppet agent!"
      # need to add 'elif' statement for more Linux distros

      ln -sf /opt/puppetlabs/puppet/bin/wrapper.sh /usr/bin/facter
      ln -sf /opt/puppetlabs/puppet/bin/wrapper.sh /usr/bin/hiera
      ln -sf /opt/puppetlabs/puppet/bin/wrapper.sh /usr/bin/puppet

    else
        printf "Curent Linux distro is not supported\n"
        exit 1
    fi

  ;;
  FreeBSD)
    printf "Current system based on Freebsd, checking packages\n"
    PVERSION=`puppet --version`
    RETVAL=$?
    if [ "$RETVAL" -ne 0 ];then
      printf "Puppet is not installed. Exiting.."
      exit 1
    fi
  ;;
  *)
    printf "Error: Unknown OS $1"
    exit 1
  ;;
  esac
  mkdir -p ${FACTS_DIR}
}
check_virtualization()
{
    which facter >/dev/null
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        VIRTUALIZATION=`facter virtual`
        if [ "$VIRTUALIZATION" == "openvz" ];then
            # Validate that iptables working
            iptables -t nat -L >/dev/null 2>&1
            if [ $? -ne 0 ];then
                echo "***********************************"
                echo "WARNING: your system based on OpenVZ container and it does not support iptables NAT table"
                echo "You need to reconfigure your OpenVZ host before running Vivabill"
                echo "Read more: https://confluence.billing-viva.com/pages/viewpage.action?pageId=1540593&moved=true"
                echo "***********************************"
                exit 1
            fi
        fi
    else
        echo "ERROR: Facter binaries are not installed!"
        exit 1
    fi
    echo "System virtualization is '$VIRTUALIZATION'"

}

check_puppet_connection()
{
    which curl >/dev/null
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        RESPONSE=$(curl -m 30 -s -k https://${PUPPET_IP}:${PUPPET_PORT}/status/v1/services | grep -Po '"state":.*?[^\\]"' | uniq)
        if [ "x${RESPONSE}" != "x\"state\":\"running\"" ];then
            echo "Puppet server at https://${PUPPET_IP}:${PUPPET_PORT} is not reachable!"
            echo "Check you network connection, or there might be a problem with Puppet server"
            echo "Stopping script."
            exit 1
        else
            echo "GOOD connection to puppet server."
        fi
    else
        echo "Curl is not installed, cannot validate connection to puppet server."
        read -e -p "Do you want to continue without Curl? (not recomended!) [Y/N]:" input
        if [ "$input" != "yes" ] || [ "$input" != "YES" ] || [ "$input" != "y" ] || [ "$input" != "Y" ]; then
            echo "Good choice!"
            exit 0
        fi
    fi
}


generate_facts()
{
  case `uname` in
  Linux)
    if [ -z `facter viva_uuid` ]; then
      uuid=`cat /proc/sys/kernel/random/uuid`
      echo "Generated new viva_uuid=$uuid"
      printf "#!/bin/sh\n\necho 'viva_uuid=$uuid'\n" > ${FACTS_DIR}/viva-uuid.sh
      chmod 700 ${FACTS_DIR}/viva-uuid.sh
    fi
  ;;
  FreeBSD)
    if [ -z `${FACTS_DIR}/viva-uuid.sh`  ]; then
      uuid=`uuidgen`
      echo "Generated new viva_uuid=$uuid"
      printf "#!/bin/sh\n\necho 'viva_uuid=$uuid'\n" > ${FACTS_DIR}/viva-uuid.sh
      chmod 700 ${FACTS_DIR}/viva-uuid.sh
    fi
  ;;
  *)
    printf "Error: Unknown OS $1"
    exit 1
  ;;
  esac
}
set_domain()
{
    echo "***********************************"
    echo "IMPORTANT: You need to configure unique HOSTNAME and DOMAIN"
    echo "***********************************"
    CURRENT_HOSTNAME=`hostname`
    CURRENT_DOMAIN=`cat /etc/resolv.conf | grep ^search | awk {'print $2'}`
    read -e -p "Hostname [$CURRENT_HOSTNAME]:" NEW_HOSTNAME; [ -z "$NEW_HOSTNAME" ] && NEW_HOSTNAME="$CURRENT_HOSTNAME"
    read -e -p "Domain [$CURRENT_DOMAIN]:" NEW_DOMAIN; [ -z "$NEW_DOMAIN" ] && NEW_DOMAIN="$CURRENT_DOMAIN"

    if [ "$CURRENT_HOSTNAME" != "$NEW_HOSTNAME" ];then
        hostname $NEW_HOSTNAME
        hostnamectl set-hostname $NEW_HOSTNAME
        if [ -f /etc/hostname ]; then
            echo "$NEW_HOSTNAME" > /etc/hostname
        fi
        if [ -f /etc/sysconfig/network ]; then
            SED_ARG="-i 's/HOSTNAME=.*/HOSTNAME=$NEW_HOSTNAME/g' /etc/sysconfig/network"
            eval sed $SED_ARG
        fi
        rm -rf  /etc/puppetlabs/puppet/ssl/
        rm -rf /var/lib/puppet/ssl/
        if [ PUPPET_ENV == "Rocky" ]; then
            systemctl restart NetworkManager.service
        else
            service network restart
        sleep 5
        fi
    fi

    if [ "x$NEW_DOMAIN" == 'x' ];then
        echo "ERROR: Domain cannot be empty!"
        exit 1
    fi

    if [ "$NEW_DOMAIN" != "$CURRENT_DOMAIN" ]; then
        if grep -q '^search' /etc/resolv.conf; then
            SED_ARG="-i 's/search.*/search $NEW_DOMAIN/g' /etc/resolv.conf"
            eval sed $SED_ARG
        else
            echo search $NEW_DOMAIN >> /etc/resolv.conf
        fi
    fi
}

create_API_AUTH_CONF()
{
  [ ! -d $VIVA_HOME ] && mkdir -p $VIVA_HOME
  if [ -f $API_AUTH_CONF ]; then
    printf "Configuration file found\n"
    read -e -p "Do you want to override current vivabill configuration [Y/N]:" input
    param_uuid_old=`cat $API_AUTH_CONF | grep -Po  '"uuid":.*?[^\\\]"' | awk -F '"' '{print $4}'`
    param_conn_old=`cat $API_AUTH_CONF | grep -Po  '"url":.*?[^\\\]"' | awk -F '"' '{print $4}'`
    param_apiv_old=`cat $API_AUTH_CONF | grep -Po  '"api":.*?[^\\\]"' | awk -F '"' '{print $4}'`
    param_user_old=`cat $API_AUTH_CONF | grep -Po  '"username":.*?[^\\\]"' | awk -F '"' '{print $4}'`
    param_pass_old=`cat $API_AUTH_CONF | grep -Po  '"password":.*?[^\\\]"' | awk -F '"' '{print $4}'`
  else
    input='y'
  fi

  if [ "$input" == "yes" ] || [ "$input" == "YES" ] || [ "$input" == "y" ] || [ "$input" == "Y" ]; then
    printf "\nPlease type new values for vivabill configuration or press ENTER to skip\n"
    [ -z "$param_uuid_old" ] && param_uuid_old=`${FACTS_DIR}/viva-uuid.sh | cut -d '=' -f2`
    [ -z "$param_conn_old" ] && param_conn_old='https://api.billing-viva.com'
    [ -z "$param_apiv_old" ] && param_apiv_old='1'
    read -e -p "Machine UUID [$param_uuid_old]:" param_uuid; [ -z "$param_uuid" ] && param_uuid="$param_uuid_old"
    read -e -p "API server URL [$param_conn_old]:" param_conn; [ -z "$param_conn" ] && param_conn="$param_conn_old"
    read -e -p "API protocol version [$param_apiv_old]:" param_apiv; [ -z "$param_apiv" ] && param_apiv="$param_apiv_old"
    read -e -p "API username [$param_user_old]:" param_user; [ -z "$param_user" ] && param_user="$param_user_old"
    read -e -p "API password [$param_pass_old]:" param_pass; [ -z "$param_pass" ] && param_pass="$param_pass_old"

    which jq >/dev/null
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        # Validate JSON
        printf "{\n\t\"uuid\":\"$param_uuid\",\n\t\"connect\":{\n\t\t\"url\":\"$param_conn\",\n\t\t\"api\":\"$param_apiv\",\n\t\t\"username\":\"$param_user\",\n\t\t\"password\":\"$param_pass\"\n\t}\n}" | jq . >/dev/null
    else
        echo "Warning jq is not installed, cannot validate API_AUTH_CONF"
    fi

    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
      printf "{\n\t\"uuid\":\"$param_uuid\",\n\t\"connect\":{\n\t\t\"url\":\"$param_conn\",\n\t\t\"api\":\"$param_apiv\",\n\t\t\"username\":\"$param_user\",\n\t\t\"password\":\"$param_pass\"\n\t}\n}" > $API_AUTH_CONF
      printf "#!/bin/sh\n\necho 'viva_uuid=$param_uuid'\n" > ${FACTS_DIR}/viva-uuid.sh
      chown root:root $API_AUTH_CONF
      chmod 600 $API_AUTH_CONF
    else
      echo "JSON validation failed! Please check information that you entered!"
      exit 1
    fi
  fi
}

check_root
check_puppet_connection
install_puppet
check_virtualization
generate_facts
set_domain
create_API_AUTH_CONF

printf "Lanching autoconfiguration service. Please wait...\n\n"
sleep 5
puppet agent --server ${PUPPET_IP} --environment=${PUPPET_ENV} -t